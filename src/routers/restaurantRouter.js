import express from "express"
import {addRatingRestaurant, editRating, getLikeList, getRatingList, likeRestaurant, placeOrder } from "../controllers/restaurantController.js"

const restaurantRouter = express.Router()
restaurantRouter.get('/get-like-list', getLikeList)
restaurantRouter.get('/get-rating-list', getRatingList)
restaurantRouter.post('/post-like',likeRestaurant)
restaurantRouter.post('/post-rate',addRatingRestaurant)
restaurantRouter.post('/order',placeOrder)
restaurantRouter.put('/update-rate',editRating)
export default restaurantRouter
