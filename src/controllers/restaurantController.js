import sequelize from "../models/index.js";
import initModels from "../models/init-models.js";
import { successCode, errorCode, failCode } from "../config/response.js";

const models = initModels(sequelize);

const getLikeList = async (req, res) => {
  try {
    const userId = req.userId; // Assuming you have the user ID available in the request object

    const data = await models.restaurant.findAll({
      include: ["user_id_users"],
    });

    const formattedData = data.map((restaurant) => {
      const usersLiked = [];
      const { user_id_users: user_id_users } = restaurant;

      user_id_users.forEach((user) => {
        const likeStatus = user.user_id === userId ? "unlike" : "like";

        usersLiked.push({
          user_id: user.user_id,
          res_id: user.res_id,
          date_like: user.date_like,
          like_status: likeStatus,
        });
      });

      return {
        res_id: restaurant.res_id,
        res_name: restaurant.res_name,
        image: restaurant.image,
        desc: restaurant.desc,
        user_id_users: usersLiked,
      };
    });

    successCode(res, formattedData, "Successfully loaded data");
  } catch (error) {
    errorCode(res, "Fail to load data");
    console.log(error);
  }
};

const likeRestaurant = async (req, res) => {
  try {
    const { userId, resId } = req.body;

    // Check if the user has already liked the restaurant
    const existingLike = await models.like_res.findOne({
      where: {
        user_id: userId,
        res_id: resId,
      },
    });

    if (existingLike) {
      // The user has already liked the restaurant, so update the like_status to false (unliked)
      models.like_res.destroy({
        where: {
          user_id: userId,
          res_id: resId,
        },
      });

      successCode(res, { liked: false }, "Restaurant unliked successfully");
    } else {
      // The user has not liked the restaurant yet, so insert a new like entry
      await models.like_res.create({
        user_id: userId,
        res_id: resId,
        date_like: new Date(),
      });

      successCode(res, { liked: true }, "Restaurant liked successfully");
    }
  } catch (error) {
    errorCode(res, "Failed to like/unlike the restaurant");
    console.log(error);
  }
};
const getRatingList = async (req, res) => {
  try {
    let data = await models.restaurant.findAll({
      include: ["user_id_user_rate_res"],
    });
    const modifiedData = data.map((restaurant) => {
      const usersRating = [];

      restaurant.user_id_user_rate_res.forEach((user) => {
        usersRating.push({
          user_id: user.user_id,
          res_id: user.res_id,
          amount: user.rate_res.amount,
          date_rate: user.rate_res.date_rate,
        });
      });

      return {
        res_id: restaurant.res_id,
        res_name: restaurant.res_name,
        image: restaurant.image,
        desc: restaurant.desc,
        user_id_user_rate_res: usersRating,
      };
    });

    successCode(res, modifiedData, "Successfully loaded data");
  } catch (error) {
    errorCode(res, "BE fail");
  }
};

const addRatingRestaurant = async (req, res) => {
  try {
    const { userId, resId, amount } = req.body;
    const rating = await models.rate_res.findOne({
      where: {
        user_id: userId,
        res_id: resId,
      },
    });
    if (rating) {
      failCode(res, "", "Already existed rating");
      return; // Add this return statement to exit the function
    } else {
      await models.rate_res.create({
        user_id: userId,
        res_id: resId,
        amount: amount,
        date_rate: new Date(),
      });
    }
    successCode(res, amount, "Restaurant rated successfully");
  } catch (error) {
    console.log(error);
    errorCode(res, "BE fail");
  }
};

const editRating = async (req, res) => {
  try {
    const { userId, resId, amount } = req.body;
    let existingRating = await models.rate_res.findOne({
      where: {
        user_id: userId,
        res_id: resId,
      },
    });

    if (existingRating) {
      await models.rate_res.update(
        { amount },
        {
          where: {
            user_id: userId,
            res_id: resId,
          },
        }
      );
      successCode(res, amount, "Rating updated successfully");
    } else {
      failCode(res, "", "Not found");
    }
  } catch (error) {
    console.log(error);
    errorCode(res, "BE fail");
  }
};

const placeOrder = async (req, res) => {
  try {
    const { userId, foodId, quantity, arr_sub_id } = req.body;

    // Check if the user and food exist
    const user = await models.user.findByPk(userId);
    const food = await models.food.findByPk(foodId);

    if (!user || !food) {
      return failCode(res, "", "User or food not found");
    }

    // Convert arr_sub_id into an array if it's not already
    const subIds = Array.isArray(arr_sub_id) ? arr_sub_id : [arr_sub_id];

    // Retrieve the sub_food items based on the subIds
    const subFoods = await models.sub_food.findAll({
      where: {
        sub_id: subIds,
      },
      attributes: ['sub_id', 'sub_name'], // Specify the attributes to retrieve
    });

    // Map the retrieved sub_foods to an object with sub_id as key and sub_name as value
    const subFoodsMap = subFoods.reduce((map, subFood) => {
      map[subFood.sub_id] = subFood.sub_name;
      return map;
    }, {});

    // Check if there is an existing order
    const existingOrder = await models.order.findOne({
      where: {
        user_id: userId,
        food_id: foodId,
      },
    });

    if (existingOrder) {
      // Update the existing order with the new quantity and sub_ids
      existingOrder.amount = quantity;
      existingOrder.arr_sub_id = JSON.stringify(subIds); // Serialize the array to a string
      await existingOrder.save();

      // Include the food's name, user's name, and sub_food names in the order response
      const orderWithDetails = {
        ...existingOrder.toJSON(),
        food_name: food.food_name, // Include the food's name
        user_name: user.user_name, // Include the user's name
        sub_food_names: subIds.map(subId => subFoodsMap[subId]), // Include the sub_food names
      };

      return successCode(res, orderWithDetails, "Order updated successfully");
    }

    // Create a new order
    const order = await models.order.create({
      user_id: userId,
      food_id: foodId,
      amount: food.amount, // Include the amount from the food
      code: food.code, // Include any additional fields from the food
      arr_sub_id: JSON.stringify(subIds), // Serialize the array to a string
      amount: quantity, // Set the quantity as the amount field
      // Add any additional order details here
    });

    // Include the food's name, user's name, and sub_food names in the order response
    const orderWithDetails = {
      ...order.toJSON(),
      food_name: food.food_name, // Include the food's name
      user_name: user.user_name, // Include the user's name
      sub_food_names: subIds.map(subId => subFoodsMap[subId]), // Include the sub_food names
    };

    // Success response
    successCode(res, orderWithDetails, "Order placed successfully");
  } catch (error) {
    console.log(error);
    errorCode(res, "BE fail");
  }
};







export {
  placeOrder,
  getLikeList,
  likeRestaurant,
  addRatingRestaurant,
  editRating,
  getRatingList,
};
