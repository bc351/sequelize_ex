import express from "express"
import rootRouter from "./routers/rootRouter.js"

const app = express()


app.use(express.json())
app.listen(8080)
app.use('/api',rootRouter)

