import { Sequelize } from "sequelize";


const sequelize = new Sequelize('db_food','root','1234',{
    host:'localhost',
    dialect:'mysql',
    port: '3306'
})
try {
    await sequelize.authenticate();
    console.log("Successful connected"); 
 } catch (error) {
     console.log("Fail to connect");
 }
export default sequelize
// yarn sequelize-auto -h localhost -d db_food -u root -x 1234 -p 3306 --dialect mysql -o src/models -l esm
